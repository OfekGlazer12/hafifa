﻿using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;

namespace Ofek___c_ {

    class Race {
        public const int numOfAnimals = 5;
        public const int numOfSteps = 10;

        private Animal[] board;
        private readonly object boardLock = new object ();

        public Race (Animal[] animalBoard) {
            this.board = animalBoard;
        }

        // public void makeStepAsync () {
        //     // lock (boardLock) {
        //     foreach (var animal in board) {
        //         animal.Steps = animal.Steps + step ();
        //     }
        //     // }
        // }

        public void TonOfTime () {
            int i;
            for (i = 0; i < 100_000_000; i++) { }
        }

        public void makeStepAsync (int index) {
            // if(index == 4){this.TonOfTime();}
            lock (boardLock) {
                this.board[index].Steps = this.board[index].Steps + step ();
            }
        }

        public async Task makeSteps () {

            var tasks = new Task[Race.numOfAnimals];
            for (int index = 0; index < Race.numOfAnimals; index++) {
                // tasks[index] = Task.Run (() => makeStepAsync (index));
                await Task.Run (() => makeStepAsync (index));
            }
            // await Task.WhenAll (tasks);
        }

        public Boolean isRaceFinished () {
            lock (boardLock) {
                foreach (var animal in board) {
                    if (animal.Steps >= numOfSteps) { return true; }
                }
                return false;
            }
        }

        public async Task printRace () {

            lock (boardLock) {
                foreach (var animal in board) {
                    System.Console.Write ($"Name: {animal.Name} : ");
                    for (int i = 0; i < animal.Steps; i++) {
                        Console.Write ("-");
                    }
                    Console.Write ($"  Steps: {animal.Steps}");
                    System.Console.WriteLine ();
                }
            }
            await Task.Delay (500);
        }

        public string getWiningAnimal () {

            string winners = "";

            foreach (var animal in board) {
                if (animal.Steps >= numOfSteps) { winners += animal.Name + " "; }
            }
            return winners;
        }

        public async Task Run () {

            while (!this.isRaceFinished ()) {
                // Task task = Task.Run (() => this.makeSteps ());
                await Task.Run (() => this.makeSteps ());
                await this.printRace ();
                Console.Clear ();
            }

        }

        private int step () {
            Random rnd = new Random ();
            int chance = rnd.Next (1, 100);
            if (chance <= 50) {
                return 1;
            }
            return 0;
        }

    }

    class Animal {
        public int Steps { get; set; }

        public string Name { get; set; }

        public Animal (int steps, string name) {
            this.Steps = steps;
            this.Name = name;
        }
    }

    class Program {

        // public static async Task<string> BoilWater () {
        //     System.Console.WriteLine ("Take the boiler out");
        //     System.Console.WriteLine ("Waiting...");
        //     await Task.Delay (100);
        //     System.Console.WriteLine ("boiler FINISHED");
        //     return "water";
        // }

        // public static void TonOfTime () {
        //     int i;
        //     for (i = 0; i < 100_000_000; i++) { }
        // }

        // public static async Task makeTea () {
        //     var boilingTask =  BoilWater ();
        //     System.Console.WriteLine ("take a cup out");
        //     TonOfTime ();
        //     System.Console.WriteLine ("put tea");
        //     var water = await boilingTask;
        //     System.Console.WriteLine ("finished tea with {0}", water);
        // }

        static async Task Main (string[] args) {

            Animal[] animals = new Animal[Race.numOfAnimals];

            // Create animals to race
            for (int i = 0; i < Race.numOfAnimals; i++) {
                Animal currAnimal = new Animal (0, $"Animal:{i+1}");
                animals[i] = currAnimal;
            }

            Race mainRace = new Race (animals);

            await mainRace.Run ();

            //  Print to show results
            await mainRace.printRace ();

            System.Console.WriteLine ($"\n Winning Animal: {mainRace.getWiningAnimal()}");

            // await makeTea ();

        }

    }
}